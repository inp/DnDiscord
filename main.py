import datetime
from pathlib import Path
from typing import List, Union

import discord
from dndiscord.config import Config
from dndiscord.plugin_manager import PluginManager


class DnDiscord(discord.Client):
    def __init__(self, plugin_path="plugins/"):

        PluginManager.client = self
        PluginManager.read_plugins(Path(plugin_path))
        super().__init__()

    async def on_ready(self,) -> None:
        await PluginManager.on_ready()
        print("Logged in as {}, ID: {}\n".format(self.user.name, self.user.id) + "-" * 88)

    async def on_resumed(self,) -> None:
        pass

    async def on_error(self, event: str, *args, **kwargs) -> None:
        pass

    async def on_message(self, message: discord.Message) -> None:
        await PluginManager.on_message(message)

    async def on_socket_raw_receive(self, message: Union[str, bytes]) -> None:
        pass

    async def on_socket_raw_send(self, payload: Union[str, bytes]) -> None:
        pass

    async def on_message_delete(self, message: discord.Message) -> None:
        pass

    async def on_message_edit(self, before: discord.Message, after: discord.Message) -> None:
        pass

    async def on_message_add(
        self, reaction: discord.Reaction, user: Union[discord.User, discord.Member]
    ) -> None:
        pass

    async def on_reaction_remove(
        self, reaction: discord.Reaction, user: Union[discord.User, discord.Member]
    ) -> None:
        pass

    async def on_reaction_clear(self, message: discord.Message, reactions: List[discord.Reaction]) -> None:
        pass

    async def on_channel_delete(self, channel: Union[discord.TextChannel, discord.VoiceChannel]) -> None:
        pass

    async def on_channel_create(self, channel: Union[discord.TextChannel, discord.VoiceChannel]) -> None:
        pass

    async def on_channel_update(
        self,
        before: Union[discord.TextChannel, discord.VoiceChannel],
        after: Union[discord.TextChannel, discord.VoiceChannel],
    ) -> None:
        pass

    async def on_member_join(self, member: discord.Member) -> None:
        pass

    async def on_member_remove(self, member: discord.Member) -> None:
        pass

    async def on_member_update(self, before: discord.Member, after: discord.Member) -> None:
        pass

    async def on_server_join(self, server: discord.Guild) -> None:
        pass

    async def on_server_remove(self, server: discord.Guild) -> None:
        pass

    async def on_server_update(self, before: discord.Guild, after: discord.Guild) -> None:
        pass

    async def on_server_role_create(self, role: discord.Role) -> None:
        pass

    async def on_server_role_delete(self, role: discord.Role) -> None:
        pass

    async def on_server_role_update(self, before: discord.Role, after: discord.Role) -> None:
        pass

    async def on_server_emojis_update(self, before: List[discord.Emoji], after: List[discord.Emoji]) -> None:
        pass

    async def on_server_available(self, server: discord.Guild) -> None:
        pass

    async def on_server_unavailable(self, server: discord.Guild) -> None:
        pass

    async def on_voice_state_update(self, before: discord.Member, after: discord.Member) -> None:
        pass

    async def on_member_ban(self, member: discord.Member) -> None:
        pass

    async def on_member_unban(self, server: discord.Guild, user: discord.User) -> None:
        pass

    async def on_typing(
        self, channel: discord.TextChannel, user: Union[discord.User, discord.Member], when: datetime.datetime
    ) -> None:
        pass

    async def on_group_join(
        self, channel: Union[discord.TextChannel, discord.VoiceChannel], user: discord.User
    ) -> None:
        pass


try:
    dndiscord = DnDiscord().run(Config().get_config()["token"])
except KeyError as e:
    print("No Token in Config File")
