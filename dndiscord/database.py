from typing import Any, Dict, Optional

from peewee import (
    BooleanField,
    CharField,
    ForeignKeyField,
    IntegerField,
    Model,
    MySQLDatabase,
    OperationalError,
)

from dndiscord.config import Config


class DatabaseConfigException(Exception):
    """Exception for Config 'database' not being accessible during connection"""


try:
    cfg: Optional[Dict[str, Any]] = Config().get_config()
except FileNotFoundError:
    raise DatabaseConfigException("Error, Config is not found")

if cfg is not None:
    cfg_db = cfg["database"]
    if cfg_db is not None:
        try:
            database: MySQLDatabase = MySQLDatabase(
                database=cfg_db["db"], host=cfg_db["host"], user=cfg_db["user"], password=cfg_db["password"]
                )
        except KeyError as e:
            raise DatabaseConfigException(
                "MySQLDatabase Error: {} does not exist in the config YAML under '{}'".format(
                    e, list(cfg.keys())[list(cfg.values()).index(cfg_db)]
                    )
                ) from e
    else:
        raise DatabaseConfigException("Database settings are None")
else:
    raise DatabaseConfigException("Database settings are None")

try:
    database.connect()
except OperationalError as e:
    raise e


class Base(Model):
    class Meta:
        database = database


class DiscordUser(Base):
    id = CharField(primary_key=True, max_length=128)
    bot = BooleanField(default=False)


class Channel(Base):
    id = CharField(primary_key=True, max_length=128)
    name = CharField(max_length=1024)
    category_id = IntegerField(null=True)
    channel_type = CharField(default="text")


class Message(Base):
    tts = BooleanField(default=False)
    message_type = IntegerField(default=0)
    id = CharField(primary_key=True, max_length=128)
    content = CharField(max_length=1024)
    channel = ForeignKeyField(Channel, on_delete='CASCADE', on_update='CASCADE')
    author = ForeignKeyField(DiscordUser, on_delete='CASCADE', on_update='CASCADE')


database.create_tables([DiscordUser, Channel, Message])
