import discord
from dndiscord.plugin_manager import PluginManager, command

META = {
    "name": "Shutdown",
    "version": "1.0",
    "author": "Ashley Williamson",
    "description": "Allow Admins to shut the bot down completely. DANGEROUS",
}

@command("shutdown", delete_message=True)
async def on_message(message: discord.Message) -> None:
    if message.author.guild_permissions.administrator:
        await message.channel.send("Shutting down...")
        await PluginManager.client.logout()