import discord
from dndiscord.database import Base, database
from dndiscord.plugin_manager import command, on_message, on_ready, PluginManager

from peewee import BigIntegerField

import asyncio
from typing import Tuple

META = {
    "name": "Wiki",
    "version": "0.1",
    "author": "Xandaros",
    "description": "Turn channels into community-editable channels",
}


seconds = 1
minutes = 60 * seconds
hours = 60 * minutes


class WikiChannel(Base):
    """A Channel that is a wiki"""
    channel_id = BigIntegerField(primary_key=True)
    wiki_message = BigIntegerField()


class EditingUser(Base):
    """A user that is currently editing a wiki message"""
    user_id = BigIntegerField(primary_key=True)
    channel_id = BigIntegerField()
    message_id = BigIntegerField()


def is_wiki_channel(channel_id: int) -> bool:
    return WikiChannel.select(WikiChannel.channel_id).where(WikiChannel.channel_id == channel_id).exists()


def is_editing(user_id: int) -> bool:
    return EditingUser.select(EditingUser.channel_id).where(EditingUser.user_id == user_id).exists()


def is_being_edited(message_id: int) -> bool:
    return EditingUser.select(EditingUser.channel_id).where(EditingUser.message_id == message_id).exists()


def has_permissions(member: discord.Member, channel: discord.TextChannel) -> bool:
    perms = channel.permissions_for(member)
    return perms.send_messages


async def send_wiki_message(channel: discord.TextChannel) -> discord.Message:
    wiki_message = await channel.send(embed=discord.Embed(
        title="Wiki channel",
        description=(
            "This channel is a wiki channel. "
            "Simply send a message in this channel to create a new wiki message. "
            "To edit an existing wiki message, use the \N{MEMO} reaction."
            )
        ))
    return wiki_message


async def update_wiki_message(wiki_channel: discord.TextChannel) -> None:
    wikichannel = WikiChannel.get(WikiChannel.channel_id == wiki_channel.id)
    old_wiki_message = await wiki_channel.fetch_message(wikichannel.wiki_message)
    new_wiki_message = await send_wiki_message(wiki_channel)
    wikichannel.wiki_message = new_wiki_message.id
    wikichannel.save()
    await old_wiki_message.delete()


async def get_message_link(channel_id: int, message_id: int) -> str:
    channel = await PluginManager.client.fetch_channel(channel_id)
    return f"https://discordapp.com/channels/{channel.guild.id}/{channel_id}/{message_id}"


@PluginManager.client.event
async def on_raw_reaction_add(payload: discord.RawReactionActionEvent):
    channel = await PluginManager.client.fetch_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    user = await PluginManager.client.fetch_user(payload.user_id)
    member = message.guild.get_member(user.id)

    if user.id == PluginManager.client.user.id:
        # Ignore own reactions
        return

    if not has_permissions(message.guild.get_member(user.id), channel):
        return

    await message.remove_reaction(payload.emoji, member)

    if not is_wiki_channel(channel.id):
        return

    if payload.emoji.name == "\N{MEMO}":
        editing_user = EditingUser.get_or_none((EditingUser.user_id == user.id) |
                                               (EditingUser.channel_id == channel.id))
        if editing_user:
            if editing_user.user_id == user.id:
                await user.send("You are already editing a message here: {}".format(
                    await get_message_link(editing_user.channel_id, editing_user.message_id))
                    )
            else:
                await user.send("That message is already being edited by somebody else.")
            return

        await edit_request(user, message)


async def edit_request(user: discord.User, message: discord.Message) -> None:
    editing_user = EditingUser.create(user_id=user.id, channel_id=message.channel.id, message_id=message.id)
    info_message = await message.channel.send(embed=discord.Embed(
        title="Editing messages",
        description=(
            "You will find the source code of the message you requested to edit below. "
            "Send a message with an initial draft for the new message within the next 5 minutes. "
            "Feel free to simply copy the source code of the original message.\n\n"
            "After that, you have 3 hours to edit the message to your satisfaction and submit the edit with "
            "\N{WHITE HEAVY CHECK MARK}."
            )
        ).set_footer(text=user.name + "#" + user.discriminator, icon_url=user.avatar_url))
    source_message = await message.channel.send(message.content.replace("`", "\\`").replace("*", "\\*"))

    def first_check(message: discord.Message) -> bool:
        nonlocal editing_user
        if message.author.id != editing_user.user_id:
            return False
        if message.channel.id != editing_user.channel_id:
            return False
        return True

    try:
        editable_message = await PluginManager.client.wait_for("message", check=first_check, timeout=5 * minutes)
    except asyncio.TimeoutError:
        try:
            await source_message.delete()
        except Exception:
            pass
        try:
            await info_message.delete()
        except Exception:
            pass
        editing_user.delete_instance()
        return

    try:
        await source_message.delete()
    except Exception:
        pass
    try:
        await info_message.delete()
    except Exception:
        pass

    info_message = await message.channel.send(embed=discord.Embed(
        title="Editing messages",
        description=(
            "You now have 3 hours to edit the message and commit the change with "
            "\N{WHITE HEAVY CHECK MARK}.\n\n"
            "You may also abort the process with \N{CROSS MARK}."
            )
        ).set_footer(text=user.name + "#" + user.discriminator, icon_url=user.avatar_url))

    await editable_message.add_reaction("\N{WHITE HEAVY CHECK MARK}")
    await editable_message.add_reaction("\N{CROSS MARK}")

    def second_check(reaction: discord.Reaction, user: discord.User) -> bool:
        nonlocal editing_user, editable_message
        if user.id != editing_user.user_id:
            return False
        if reaction.message.id != editable_message.id:
            return False
        if reaction.emoji != "\N{WHITE HEAVY CHECK MARK}" and reaction.emoji != "\N{CROSS MARK}":
            return False
        return True

    try:
        (reaction, user) = await PluginManager.client.wait_for("reaction_add",
                                                               check=second_check,
                                                               timeout=3 * hours)
    except asyncio.TimeoutError:
        try:
            await info_message.delete()
        except Exception:
            pass
        try:
            await editable_message.delete()
        except Exception:
            pass
        editing_user.delete_instance()
        return

    if reaction.emoji == "\N{WHITE HEAVY CHECK MARK}":
        current_message = await editable_message.channel.fetch_message(editable_message.id)
        await message.edit(content=current_message.content)

    try:
        await info_message.delete()
    except Exception:
        pass
    try:
        await editable_message.delete()
    except Exception:
        pass
    editing_user.delete_instance()


@command("wiki")
async def wiki(message: discord.Message):
    wiki_message = await send_wiki_message(message.channel)
    WikiChannel.create(channel_id=message.channel.id, wiki_message=wiki_message.id)


@on_ready
async def ready():
    database.create_tables([EditingUser, WikiChannel])


@PluginManager.client.event
async def on_raw_message_delete(payload: discord.RawMessageDeleteEvent) -> None:
    WikiChannel.delete().where(WikiChannel.wiki_message == payload.message_id).execute()


@on_message()
async def on_message(message: discord.Message) -> None:
    if message.content.startswith("!"):
        return

    if is_editing(message.author.id):
        return

    if not is_wiki_channel(message.channel.id):
        return

    wiki_message = await message.channel.send(message.content)
    await update_wiki_message(message.channel)
    await message.delete()

    await wiki_message.add_reaction("\N{MEMO}")
